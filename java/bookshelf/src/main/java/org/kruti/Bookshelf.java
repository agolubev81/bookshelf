package org.kruti;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import javax.swing.JFrame;

import org.kruti.ui.IconImageLabel;
import org.kruti.ui.IconLabel;
import org.kruti.ui.IconTextLabel;
import org.kruti.ui.WorkspacePanel;

public class Bookshelf {

  public static void main(String[] args) throws Exception {
    File appDir = null;
    if (args != null) {
      for (int i = 0; i < args.length;) {
        String arg = args[i];
        if (arg.equals("-app") && args.length>i+1) {
          appDir = new File(args[i+1]);
        }
        i=+2;
      }
    }
    if (appDir == null) {
      appDir = new File(System.getProperty("user.dir"));
    }
    File bookshelfProperties = new File(appDir, "Bookshelf.properties");
    if (!bookshelfProperties.isFile()) {
      System.out.println(bookshelfProperties+" file can't found.");
      return;
    }
    Properties properties = new Properties();
    FileInputStream fis = new FileInputStream(bookshelfProperties);
    try {
      properties.load(fis);
    } finally {
      fis.close();
    }
    String title = properties.getProperty("title");
    String backgroundImagePath = properties.getProperty("background");
    File backgroundImageFile = new File(appDir,backgroundImagePath);
    WorkspacePanel workspacePanel = WorkspacePanel.create(title, backgroundImageFile);
    String icons = properties.getProperty("icons");
    if (icons != null) {
      for(String iconName: icons.split(",")) {
        String iconImagePath = properties.getProperty(iconName+".image");
        File iconImageFile = new File(appDir, iconImagePath);
        String positionProperty = properties.getProperty(iconName+".position");
        int positionX = 0;
        int positionY = 0;
        if (positionProperty != null) {
          String[] positions = positionProperty.split(",");
          positionX = positions.length>=2 ? Integer.valueOf(positions[0]): null;
          positionY = positions.length>=2 ? Integer.valueOf(positions[1]): null;
        }
        String commandProperty = properties.getProperty(iconName+".command");
        String[] commandAndArgs = null;
        if (commandProperty != null) {
          commandAndArgs = commandProperty.split(",");
        }
        String iconTextString = properties.getProperty(iconName+".text");
        if (iconTextString == null) {
          iconTextString = String.valueOf(" ");
        }
        String iconsTextColorName = properties.getProperty("icons.text.color","RED");
        int iconsBorderThickness = Integer.valueOf(properties.getProperty("icons.border.thickness","5"));
        String iconsSelectionColorName = properties.getProperty("icons.selection.color","WHITE");
        IconText iconText = IconTextLabel.create(iconTextString, iconsTextColorName, iconsSelectionColorName);
        IconImage iconImage = IconImageLabel.create(iconImageFile, iconsBorderThickness, iconsSelectionColorName);
        IconLabel itemLabel = IconLabel.create(iconText, iconImage, positionX, positionY, commandAndArgs);
        workspacePanel.addItem(itemLabel);
      }
    }

    JFrame mainFrame = new JFrame(workspacePanel.getName());
    mainFrame.add(workspacePanel);
    mainFrame.addKeyListener(workspacePanel);
    mainFrame.setSize(workspacePanel.getWidth(), workspacePanel.getHeight());
    mainFrame.setResizable(false);
    mainFrame.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent windowEvent){
        System.exit(0);
      }
    });
    mainFrame.setUndecorated(true); // avoid title bar with min/max/close(x) buttons
    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // avoid title bar with min/max/close(x) buttons
//    mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
    mainFrame.setLocationRelativeTo(null); // positioning on center of screen 
    mainFrame.setVisible(true);
  }
}
