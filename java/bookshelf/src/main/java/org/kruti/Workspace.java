package org.kruti;

import java.util.Collection;

public interface Workspace {
  String getName();
  String getBackgroundImagePath();
  void addItem(Icon item);
  void removeItem(Icon item);
  boolean hasItem(Icon item);
  Collection<Icon> getAllItems();
  Collection<Icon> getSelectedItems();
}
