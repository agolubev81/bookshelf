package org.kruti.ui;

import java.awt.Color;

import javax.swing.JLabel;

import org.kruti.IconText;

public class IconTextLabel extends JLabel implements IconText {
  private String _iconText;
  private String _textSelectionColorName;
  private Color _textSelectionColor;
  private String _textColorName;
  private Color _textColor;

  public static IconText create(String iconText, String textColorName, String textSelectionColorName) throws Exception {
    Color textColor = ColorUtil.getColor(textColorName);
    Color textSelectionColor = ColorUtil.getColor(textSelectionColorName);
    IconTextLabel textLabel = new IconTextLabel(iconText, textColorName, textColor, textSelectionColorName, textSelectionColor);
    textLabel.setText(iconText);
    textLabel.setForeground(textColor);
    return textLabel;
  }
  
  IconTextLabel(String iconText, String textColorName, Color textColor, String textSelectionColorName, Color textSelectionColor) {
    super();
    _iconText = iconText;
    _textColorName = textColorName;
    _textColor = textColor;
    _textSelectionColorName = textSelectionColorName;
    _textSelectionColor = textSelectionColor;
  }

  public void select() {
    setForeground(_textSelectionColor);
  }
  
  public void deselect() {
    setForeground(_textColor);
  }

  @Override
  public String getIconText() {
    return _iconText;
  }

  @Override
  public String getIconTextColorName() {
    return _textColorName;
  }

  @Override
  public String getIconTextSelectionColorName() {
    return _textSelectionColorName;
  }

  @Override
  public int getIconTextHeight() {
    return 20; // default font height
  }

  @Override
  public int getIconTextWidth() {
    throw new UnsupportedOperationException("not implemented");
  }
}
