package org.kruti.ui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import org.kruti.Icon;
import org.kruti.Workspace;

@SuppressWarnings("serial")
public class WorkspacePanel extends JPanel implements Workspace, MouseListener, KeyListener {

  public static WorkspacePanel create(String name, File backgroundImageFile) throws Exception {
    final BufferedImage backgroundImage = ImageIO.read(backgroundImageFile);
    WorkspacePanel workspacePanel = new WorkspacePanel(name, backgroundImageFile.getPath(), backgroundImage);
    workspacePanel.setSize(backgroundImage.getWidth(), backgroundImage.getHeight());
    workspacePanel.setLayout(null);
    return workspacePanel;
  }
  
  private String _name;
  private String _backgroundImagePath;
  private List<IconLabel> _items = new ArrayList<IconLabel>();
  private Image _backgroundImage;
  
  private WorkspacePanel(String name, String backgroundImagePath, Image backgroundImage) {
    _name = name;
    _backgroundImagePath = backgroundImagePath;
    _backgroundImage = backgroundImage;
  }
  
  @Override
  public String getName() {
    return _name;
  }

  @Override
  public String getBackgroundImagePath() {
    return _backgroundImagePath;
  }

  @Override
  public synchronized Collection<Icon> getAllItems() {
    return Collections.<Icon>unmodifiableCollection(_items);
  }

  @Override
  public synchronized void addItem(Icon item) {
    IconLabel itemLabel = (IconLabel)item;
    itemLabel.addMouseListener(this);
    _items.add(itemLabel);
    super.add(itemLabel);
  }

  @Override
  public synchronized void removeItem(Icon item) {
    IconLabel itemLabel = (IconLabel) item;
    if (_items.remove(itemLabel)) {
      itemLabel.removeMouseListener(this);
      super.remove(itemLabel);
    }
  }

  @Override
  public synchronized boolean hasItem(Icon item) {
    return _items.contains(item);
  }

  @Override
  public synchronized Collection<Icon> getSelectedItems() {
    Collection<Icon> list = new ArrayList<Icon>();
    for(Icon item: getAllItems()) {
      if (item.isSelected()) {
        list.add(item);
      }
    }
    return list;
  }
  
  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.drawImage(_backgroundImage, 0, 0, this);
  }
  

  //----------------------------------------------------------------------------
  // MouseListener methods
  @Override
  public void mouseClicked(MouseEvent e) {
    Component component = e.getComponent();
    if (component instanceof IconLabel) {
      IconLabel itemLabel = (IconLabel) component;
      boolean executeCommand = false;
      if (itemLabel.isSelected()) {
        executeCommand = true;
      } else {
        for(Icon item : getSelectedItems()) {
          item.deselect();
        }
        itemLabel.select();
        if (e.getClickCount() > 1) {
          executeCommand = true;
        }
      }
      if (executeCommand) {
        ProcessBuilder command = new ProcessBuilder().command((String[])itemLabel.getCommand());
        try {
          command.start();
        } catch (IOException e1) {
          e1.printStackTrace();
        }
      }
    }
  }

  @Override
  public void mousePressed(MouseEvent e) {
    // TODO Auto-generated method stub
  }

  @Override
  public void mouseReleased(MouseEvent e) {
    // TODO Auto-generated method stub
  }

  @Override
  public void mouseEntered(MouseEvent e) {
    // TODO Auto-generated method stub
  }

  @Override
  public void mouseExited(MouseEvent e) {
    // TODO Auto-generated method stub
  }

  //----------------------------------------------------------------------------
  // KeyListener methods
  @Override
  public void keyTyped(KeyEvent e) {
  }

  @Override
  public void keyPressed(KeyEvent e) {
    // TODO Auto-generated method stub
  }

  @Override
  public void keyReleased(KeyEvent e) {
    if (e.getKeyCode() == KeyEvent.VK_SPACE) {
      Collection<Icon> selectedItems = getSelectedItems();
      if (!selectedItems.isEmpty()) {
        IconLabel itemLabel = (IconLabel) selectedItems.iterator().next();
        ProcessBuilder command = new ProcessBuilder().command((String[])itemLabel.getCommand());
        try {
          command.start();
        } catch (IOException e1) {
          e1.printStackTrace();
        }
      }
    } 
    else if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT) {
      Collection<Icon> selectedItems = getSelectedItems();
      Icon selectedItem = null;
      if (!selectedItems.isEmpty()) {
        selectedItem = selectedItems.iterator().next();
        for(Icon item: selectedItems) {
          item.deselect();
        }
      }
      if (selectedItem == null) {
        if (!_items.isEmpty()) {
          selectedItem = _items.get(0);
          selectedItem.select();
        }
      } else {
        int selectedItemIndex = _items.indexOf(selectedItem);
        int newSelectedItemIndex = -1;
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
          newSelectedItemIndex = selectedItemIndex - 1;
          if (newSelectedItemIndex < 0) {
            newSelectedItemIndex = _items.size()-1 ;
          }
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
          newSelectedItemIndex = selectedItemIndex + 1;
          if (newSelectedItemIndex == _items.size()) {
            newSelectedItemIndex = 0;
          }
        }
        selectedItem = _items.get(newSelectedItemIndex);
        selectedItem.select();
      }
    }  
  }
}
