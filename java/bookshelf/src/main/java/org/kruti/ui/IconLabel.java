package org.kruti.ui;

import javax.swing.BoxLayout;
import javax.swing.JComponent;

import org.kruti.Icon;
import org.kruti.IconImage;
import org.kruti.IconText;

@SuppressWarnings("serial")
public class IconLabel extends JComponent implements Icon {

  public static IconLabel create(IconText iconText, IconImage iconImage, int positionX, int positionY, Object command) {
    IconTextLabel iconTextLabel = (IconTextLabel) iconText;
    IconImageLabel iconImageLabel = (IconImageLabel) iconImage;
    IconLabel itemLabel = new IconLabel(command, iconImageLabel, iconTextLabel, positionX, positionY);
    itemLabel.setLayout(new BoxLayout(itemLabel, BoxLayout.Y_AXIS));
    itemLabel.add(iconTextLabel);
    itemLabel.add(iconImageLabel);
    int itemHeight = iconImageLabel.getIconImageHeight() + iconImageLabel.getBorderThickness() + iconTextLabel.getIconTextHeight();
    itemLabel.setBounds(positionX, positionY-itemHeight, iconImageLabel.getIconImageWidth(), itemHeight);
    return itemLabel;
  }

  private Object _command;
  private IconImageLabel _iconImage;
  private IconTextLabel _iconText;
  private boolean _selected;
  private int _x;
  private int _y;

  private IconLabel(Object command, IconImageLabel itemIcon, IconTextLabel itemText, int x, int y) {
    _command = command;
    _iconImage = itemIcon;
    _iconText = itemText;
    _selected = false;
    _x = x;
    _y = y;
  }

  @Override
  public int getPositionX() {
    return _x;
  }

  @Override
  public int getPositionY() {
    return _y;
  }
  
  @Override
  public Object getCommand() {
    return _command;
  }

  @Override
  public synchronized void select() {
    _selected = true;
    _iconImage.select();
    _iconText.select();
  }

  @Override
  public synchronized void deselect() {
    _selected = false;
    _iconImage.deselect();
    _iconText.deselect();
  }

  @Override
  public boolean isSelected() {
    return _selected;
  }

  @Override
  public IconImage getIconImage() {
    return _iconImage;
  }

  @Override
  public IconText getIconText() {
    return _iconText;
  }
}
