package org.kruti.ui;

import java.awt.Color;
import java.lang.reflect.Field;

public class ColorUtil {
  
  /**
   * Accepts one of color field name of {@link Color} or RGB comma-separated value, like "255,0,0"
   * @param color
   * @return
   */
  public static Color getColor(String color) {
    if (color.contains(",")) {
      String[] rgbColor = color.split(",");
      int[] rgb = null;
      if (rgbColor.length == 3) {
        try {
          rgb = new int[] {Integer.parseInt(rgbColor[0]),Integer.parseInt(rgbColor[1]),Integer.parseInt(rgbColor[2])};
        } catch(NumberFormatException e) {
          // do nothing
        }
      }
      if (rgb == null) {
        throw new RuntimeException("Incorrect RGB color format: "+color);
      }
      return new Color(rgb[0], rgb[1], rgb[2]);
    } else {
      try {
        Field field = Color.class.getField(color);
        return (Color)field.get(null);
      } catch (Exception e) {
        throw new RuntimeException("Specified color isn't available: "+color);
      }    
    }
  }
}
