package org.kruti.ui;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import org.kruti.IconImage;

public class IconImageLabel extends JLabel implements IconImage {
  private int _height;
  private int _width;
  private String _iconImagePath;
  private int _borderThickness;
  private String _borderSelectionColorName;
  private Color _borderSelectionColor;

  public static IconImage create(File iconImage, int borderThickness, String borderSelectionColorName) throws Exception {
    Color borderSelectionColor = ColorUtil.getColor(borderSelectionColorName);
    BufferedImage labelImage = ImageIO.read(iconImage);
    IconImageLabel imageLabel = new IconImageLabel(labelImage.getHeight(), labelImage.getWidth(), iconImage.getPath(), borderThickness, borderSelectionColorName, borderSelectionColor);
    imageLabel.setIcon(new ImageIcon(labelImage));
    imageLabel.setSize(labelImage.getWidth(), labelImage.getHeight());
    return imageLabel;
  }
  
  IconImageLabel(int height, int width, String iconImagePath, int borderWidth, String borderSelectionColorName, Color borderSelectionColor) {
    super();
    _height = height;
    _width = width;
    _iconImagePath = iconImagePath;
    _borderThickness = borderWidth;
    _borderSelectionColorName = borderSelectionColorName;
    _borderSelectionColor = borderSelectionColor;
  }

  @Override
  public int getIconImageHeight() {
    return _height;
  }

  @Override
  public int getIconImageWidth() {
    return _width;
  }

  @Override
  public String getImagePath() {
    return _iconImagePath;
  }

  @Override
  public String getBorderSelectionColor() {
    return _borderSelectionColorName;
  }

  @Override
  public int getBorderThickness() {
    return _borderThickness;
  }
  
  public void select() {
    setBorder(BorderFactory.createLineBorder(_borderSelectionColor, _borderThickness));
  }
  
  public void deselect() {
    setBorder(null);
  }
}
