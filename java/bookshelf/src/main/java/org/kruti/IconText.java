package org.kruti;

public interface IconText {
  String getIconText();
  String getIconTextColorName();
  String getIconTextSelectionColorName();
  int getIconTextHeight();
  int getIconTextWidth();
}
