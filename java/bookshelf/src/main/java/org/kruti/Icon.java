package org.kruti;

public interface Icon {
  int getPositionX();
  int getPositionY();
  String getName();
  IconImage getIconImage();
  IconText getIconText();
  Object getCommand();
  void select();
  void deselect();
  boolean isSelected();
}
