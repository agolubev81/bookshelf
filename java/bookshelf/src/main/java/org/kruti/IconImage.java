package org.kruti;

public interface IconImage {
  int getIconImageHeight();
  int getIconImageWidth();
  String getImagePath();
  String getBorderSelectionColor();
  int getBorderThickness();
}
