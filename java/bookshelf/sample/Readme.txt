This is a description of "bookshelf" application directories and files:

- "data" typical directory for user to put custom graphical images for bookshelf background and books 
- "lib" is a directory with application libraries
- "Bookshelf.properties" is a configuration file for bookshelf application
- "Bookshelf.bat" is a script file for starting bookshelf application