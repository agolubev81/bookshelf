To build and run this application you need :

1) Java 7 or higher
2) Maven
3) Set environment variable JAVA_HOME to Java installation directory
4) Run command: mvn clean install
5) Check sample directory for details or run application by command : mvn exec:java 
